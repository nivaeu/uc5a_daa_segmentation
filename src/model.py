#
# Copyright (c) Søren Rasmussen 2020 -- 2021.
# Copyright (c) The Danish Agricultural Agency 2020 -- 2021.
# This file belongs to subproject uc5a_daa_segmentation of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

from copy import deepcopy
import torch as th
import torch.nn.functional as F
from torch.optim import Adam
import pytorch_lightning as pl
from typing import Any

import torch.nn as nn
import torchvision as tv

import numpy as np

from pl_bolts.callbacks.self_supervised import BYOLMAWeightUpdate


class BYOL(pl.LightningModule):
    def __init__(self,
                 encoder,
                 learning_rate: float = 0.2,
                 weight_decay: float = 15e-6,
                 barrier_loss=False,
                 **unused_kwargs):
        '''
         BYOL module.
         Implements https://arxiv.org/abs/2006.07733

         Parameters:
         -----------
         encoder : encoder module
         learning_rate, weight_decay : passed to Adam
         barrier_loss : Bool.
            Use stabilizing barrier loss?
            Confines the entries in the normalized embeddings to a sensible range.
        '''
        super().__init__()
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        #self.save_hyperparameters()
        self.encoder = encoder
        self.online_network = EncoderProjector(self.encoder)
        self.target_network = deepcopy(self.online_network)
        self.predictor = MLP(256)
        self.weight_callback = BYOLMAWeightUpdate()
        self.barrier_loss = barrier_loss

    #def on_train_batch_end(self, epoch_end_obatch: Any, batch_idx: int, dataloader_idx: int) -> None:
    #    self.weight_callback.on_train_batch_end(self.trainer, self, batch, batch_idx, dataloader_idx)

    def forward(self, x):
        return self.encoder(x)

    def shared_step(self, batch, *args, **kwargs):
        img_1, img_2 = batch[0], batch[1]
        h1 = self.predictor(self.online_network(img_1))
        h2 = self.predictor(self.online_network(img_2))
        with th.no_grad():
            z1 = self.target_network(img_1)
            z2 = self.target_network(img_2)
            z1n = th.norm(z1,dim=1)
            z2n = th.norm(z2,dim=1)
        def normalized_mse_loss(h,z, hn, zn):
            return 2-2*((h*z).sum(-1) / (hn*zn)).mean()
            
        h1n = th.norm(h1,dim=1)
        h2n = th.norm(h2,dim=1)
        loss_a, loss_b = normalized_mse_loss(h1,z2,h1n,z2n), normalized_mse_loss(h2,z1,h2n,z1n)
        def barrier_loss_fun(x,a=.1,b=100,s=10,e=2):
            return (s*(th.relu(a-x) + th.relu(x-b))**e).mean()
        barrier_loss = 0.5 * (barrier_loss_fun(h1n) + barrier_loss_fun(h2n))
        nmse_loss = loss_a+loss_b
        total_loss = nmse_loss
        if self.barrier_loss:
            total_loss = total_loss + barrier_loss
        return total_loss, nmse_loss, barrier_loss, (h1n.mean()+h2n.mean())/2
        

    def training_step(self, batch, *args, **kwargs):
        total_loss, nmse_loss, barrier_loss, norm = self.shared_step(batch)

        # log results
        log_dict = {'nmse_loss': nmse_loss, 'barrier_loss': barrier_loss, 'norm': norm}
        return {'loss': total_loss, 'log': log_dict}

    def configure_optimizers(self):
        return Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.weight_decay)

class MLP(nn.Module):
    def __init__(self, input_dim=2048, hidden_size=4096, output_dim=256):
        super().__init__()
        self.output_dim = output_dim
        self.input_dim = input_dim
        self.model = nn.Sequential(
            nn.Linear(input_dim, hidden_size, bias=False),
            nn.BatchNorm1d(hidden_size),
            nn.ReLU(inplace=True),
            nn.Linear(hidden_size, output_dim, bias=True))

    def forward(self, x):
            return self.model(x)



class EncoderProjector(nn.Module):
    def __init__(self, encoder):
        super().__init__()

        ## Encoder
        self.encoder = encoder

        # Projector
        with th.no_grad(): #Infer number of channels
            C = encoder(th.rand(2,4,1,1)).shape[1]
        self.projector = MLP(input_dim=C)

    def forward(self, x):
        return self.projector(self.encoder(x).view(x.shape[0],-1))



def replace_resnet_conv1(model, channels):
    model.conv1 = nn.Conv2d(channels,64,7,stride=2,padding=3,bias=False)
    nn.init.kaiming_normal_(model.conv1.weight, mode='fan_out', nonlinearity='relu')
    return model


def byol_deeplabv3(channels=4, avgpool=True):
    backbone = replace_resnet_conv1(tv.models.resnet50(replace_stride_with_dilation=[False,True,True]),channels)
    backbone = nn.Sequential(*list(backbone.children())[:-2])
    head = tv.models.segmentation.deeplabv3.DeepLabHead(2048,1)
    head = nn.Sequential(*list(head.children())[:-1])
    layers = [backbone, head]
    if avgpool:
        layers.append(nn.AdaptiveAvgPool2d((1,1)))
    return nn.Sequential(*layers)


class FocalLoss(nn.Module):
    def __init__(self, gamma=2, eps=1e-6):
        super().__init__()
        self.gamma = gamma
        self.eps = eps

    def forward(self, inputs, targets, mask=None, logits=False):
        if logits: inputs = inputs.sigmoid()
        if inputs.shape != targets.shape:
            inputs = F.interpolate(inputs, size=targets.shape[-2:], mode='bilinear', align_corners=False)
        a = -(  inputs)**self.gamma * (1-inputs+self.eps).log()
        b = -(1-inputs)**self.gamma * (  inputs+self.eps).log()
        fl = (a-(a-b)*targets)
        if mask is not None:
            if mask.shape[2:] != targets.shape[2:]:
                mask = F.interpolate(mask, size=targets.shape[-2:], mode='nearest')
            C = mask.shape[1]
            #Masked mean:
            fl = (fl*mask).sum((1,2,3)) / (mask.sum((1,2,3))*C+self.eps)
            fl = fl.mean()
        else:
            fl = fl.mean()
        return fl


class SegModel(pl.LightningModule):
    def __init__(self, lr=1e-2, gamma=2, selftrained=None, ckpt=None, **unused_kwargs):
        '''
         Segmentation module.
         
         Parameters:
         -----------
         lr : ADAM learning rate
         alpha, gamma : Focal Loss parameters
         selftrained : unsupervised checkpoint path (if any)
         ckpt : supervised checkpoint path (if any)
        '''

        super().__init__()
        self.lr = lr
        self.encoder = byol_deeplabv3(channels=4, avgpool=False)
        if selftrained:
            self.load_state_dict(th.load(selftrained, th.device('cpu'))['state_dict'], strict=False)
        
        #Initialize output bias for focal loss
        #https://arxiv.org/abs/1708.02002
        p_fg = 0.01        
        self.model = nn.Sequential(self.encoder, nn.Conv2d(256,1,1))
        self.model[-1].bias.data[:] = -np.log((1-p_fg)/p_fg)
        if ckpt:
            self.load_state_dict(th.load(ckpt, th.device('cpu'))['state_dict'], strict=False)
        self.lossfun = FocalLoss(gamma=gamma)

    def forward(self, x, postproc=True):
        y = self.model(x)
        if postproc:
            y = F.interpolate(y.sigmoid(), size=x.shape[-2:], mode='bilinear', align_corners=False)
        return y

    def training_step(self, batch, batch_idx):
        img, target, mask = batch
        out = self(img)
        loss_val = self.lossfun(out, target, mask=mask)
        log_dict = {'train_loss': loss_val}
        return {'loss': loss_val, 'log': log_dict}

    def validation_step(self, batch, batch_idx):
        img, target, mask = batch
        out = self(img)
        loss_val = self.lossfun(out, target, mask=mask)

        iou_val = iou(out, mask, .5)  
        if batch_idx == 0:
            Nimg = 8
            denorm = tv.transforms.Normalize([-0.485/0.229, -0.456/0.224, -0.406/0.225], [1/0.229, 1/0.224, 1/0.225])
            imgs_denorm = th.stack([denorm(i[:3]) for i in img[:Nimg]])
            self.logger.experiment.add_images("val_imgs", imgs_denorm)
            self.logger.experiment.add_images("val_out", out[:Nimg].sigmoid())
            self.logger.experiment.add_images("val_tgt", target[:Nimg].float())
            self.logger.experiment.add_images("val_mask", mask[:Nimg].float())
            self.logger.experiment.flush()
        self.log('val_iou', iou_val, on_step=False, on_epoch=True, prog_bar=False, logger=True)

        return {'val_loss': loss_val}

    def test_step(self, batch, batch_idx):
        img, mask = batch
        out = self(img)
        loss_test = self.lossfun(out, mask)
        iou_test = iou(out, mask, .5)
        metrics = {'test_loss': loss_test, 'test_iou': iou_test}
        self.log_dict(metrics)
        

    def validation_epoch_end(self, outputs):
        loss_val = th.stack([x['val_loss'] for x in outputs]).mean()
        log_dict = {'val_loss': loss_val}
        return {'log': log_dict, 'val_loss': log_dict['val_loss'], 'progress_bar': log_dict}

    def configure_optimizers(self):
        opt = th.optim.Adam(self.model.parameters(), lr=self.lr)
        return opt



def iou(pred, target, thr=.5, logits=False, mask=None):
    #Intersection over union
    if mask is None:
        mask = th.ones(pred.shape, dtype=th.bool)
    p = pred
    if logits: p = p.sigmoid()

    p = p[mask].view(-1) > thr
    t = target[mask].view(-1)
    I = (p*t).sum()
    U = (p+t).bool().sum()
    L = mask.sum()
    return .5*(I.float()/U + (L-U).float()/(L-I))

# def iou(pred, target, thr=.5, logits=False):
#     #Intersection over union
#     p = pred
#     if logits: p = p.sigmoid()
#     p = p.view(-1) > thr
#     t = target.view(-1)
#     I = (p*t).sum()
#     U = (p+t).bool().sum()
#     L = len(p)
#     return .5*(I.float()/U + (L-U).float()/(L-I))
