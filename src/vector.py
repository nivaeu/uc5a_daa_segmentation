#
# Copyright (c) Søren Rasmussen 2020 -- 2021.
# Copyright (c) The Danish Agricultural Agency 2020 -- 2021.
# This file belongs to subproject uc5a_daa_segmentation of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

import numpy as np
from shapely.geometry import shape, Point, MultiPoint, Polygon, MultiPolygon, GeometryCollection
from shapely.strtree import STRtree
import fiona as fio
import matplotlib.pyplot as plt
import itertools

def random_point_in_bbox(bbox):
        x = np.random.uniform(bbox[0],bbox[2])
        y = np.random.uniform(bbox[1],bbox[3])
        return Point(x,y)  

def fiocollection2multipolygon(c):
    #Convert fiona collection to a single multipolygon
    polys = []
    for mp in c:
        if (isinstance(mp,dict)) and (g := mp.get('geometry')):
            shapes = shape(g)
            if isinstance(shapes, MultiPolygon):
                polys += shapes
            elif isinstance(shapes, Polygon):
                polys.append(shapes)
            else:
                raise ValueError(f"Unknown polygon format {type(shapes)}")
    return MultiPolygon(polys)
    
def process_area(area, buffer=None, simplify=None):
    '''
      Make sure area is a MultiPolygon.
      Load from file if needed.
      Buffer and simplify if needed.
    '''
    if isinstance(area,(list,tuple)):
        if len(area) == 2 and not isinstance(area[0],Polygon):
            #Assume we have area = (path, crs)
            with fio.open(area[0], crs=area[1]) as c:
                area = fiocollection2multipolygon(c)
        else: #Assume area is a list of polygons
            area = MultiPolygon(area)
    elif not isinstance(area, (Polygon, MultiPolygon)):
        raise ValueError("Invalid area. See PointSampler docstring.")

    if buffer not in (None,False):   area = area.buffer(buffer)
    if simplify not in (None,False): area = area.simplify(simplify)
    return area



class PointSampler():
    '''
     Sample random point(s) from a list of points
     Also acts as a parent class for other PointSamplers
          
     Parameters
     ----------
         points : list or MultiPoint
     Returns
     -------
         (list of) point(s)
     Example
     -------
        P = PointSampler(points)
        pt0 = P() #Random point
        (pt1, pt2, pt3) = P(size=3) #3 random points
        pt4 = P(4) #Point number 4
    '''
    _visualization_attrs = ('points')
    def __init__(self, points):
        is_multipoint = isinstance(points, MultiPoint)
        is_listofpoint = isinstance(points, list) and isinstance(points[0], Point)
        assert is_multipoint or is_listofpoint, f'`points` must be `MultiPoint` or `list(Point)`. Got {type(points)}.'
        self.points = MultiPoint(points)

    def __len__(self): return len(self.points)

    def __call__(self,idx=None, n=None):
        size = 1 if n is None else n
        if idx is None:
            idx = np.random.choice(len(self), size=size)
            res = MultiPoint([self.points[i] for i in idx])
        else:
            res = self.points[idx]
        return res
    def __getitem__(self, idx): return self(idx)

    @property
    def bounds(self): return self.points.bounds

    def visualize(self):
        collection = [getattr(self,n) for n in self._visualization_attrs]
        assert len(collection) > 0, "Nothing to visualize"
        return GeometryCollection(collection)

class CentroidPointSampler(PointSampler):
    '''
     Sample random point(s) from centroids of a list of polygons
          
     Parameters
     ----------
         valid_area : list or MultiPolygon
     Returns
     -------
         (list of) point(s)
     Example
     -------
        P = CentroidPointSampler(points)
        pt0 = P() #Random point
        (pt1, pt2, pt3) = P(size=3) #3 random points
        pt4 = P(4) #Point number 4
    '''
    _visualization_attrs = ('polygons', 'points')
    def __init__(self, polygons):
        self.polygons = (polygons if isinstance(polygons,MultiPolygon) else process_area(polygons))
        super().__init__(points=[p.centroid for p in self.polygons])

class PolygonPointSampler(PointSampler):
    '''
     Sample random point(s) inside a MultiPolygon.
     To speed up the process, this is done as follows:
         1. Randomly select a polygon from the MultiPolygon
            (weighted by area).
         2. Generate random points within the polygon BBOX
            until one falls inside the polygon.
     
     Parameters
     ----------
         valid_area : Polygons covering valid area.
             shapely.geometry.MultiPolygon
             or
             Tuple of the form (filename, crs)
         buffer : Buffering radius (see shapely documentation)
         simplify : Simplification radius (see shapely.geometry.Polygon)
     Returns
     -------
         (list of) point(s)
     Example
     -------
        P = PolygonPointSampler(multipolygon)
        pt0 = P() #Random point
        (pt1, pt2, pt3) = P(size=3) #3 random points
        pt4 = P(4) #Point from polygon number 4
    '''
    _visualization_attrs = ('valid_area',)
    def __init__(self, valid_area, buffer=None, simplify=None):
        self.valid_area = (valid_area if isinstance(valid_area,MultiPolygon) else process_area(valid_area, buffer, simplify))
        areas = np.array([s.area for s in self.valid_area])
        self.p = areas/areas.sum()
    
    def __len__(self): return len(self.valid_area)

    def __call__(self,idx=None, n=None):
        size = 1 if n is None else n
        if idx is None:
            idx = np.random.choice(len(self.p), size=size, p=self.p)
            shapes = [self.valid_area[i] for i in idx]
        else:
            shapes = [self.valid_area[idx]]
        points = []
        for s in shapes:
            pt = random_point_in_bbox(s.bounds)
            while not s.contains(Point(pt)):
                pt = random_point_in_bbox(s.bounds)
            points.append(pt)
        return points[0] if n is None else points
    
    @property
    def bounds(self): return self.valid_area.bounds()

class GridPointSampler(PointSampler):
    '''
     Sample random point(s) from a grid, covering a MultiPolygon.
          
     Parameters
     ----------
         valid_area : Polygons covering valid area.
             shapely.geometry.MultiPolygon
             or
             Tuple of the form (filename, crs)
         dx, dy : Distance between points in x- and y-direction (ignored if Multi)
         ox, oy : Offsets in x- and y-direction
         offset_mode : 'rel' or 'abs'
            'rel' means offsets are relative to distances.
            'abs' means offsets are absolute.
         buffer : Buffering radius (see shapely documentation)
         simplify : Simplification radius (see shapely.geometry.Polygon)
     Returns
     -------
         (list of) point(s)
     Example
     -------
        P = GridPointSampler(multipolygon)
        pt0 = P() #Random point
        (pt1, pt2, pt3) = P(size=3) #3 random points
        pt4 = P(4) #Point number 4
    '''
    _visualization_attrs = ('valid_area', 'points')
    def __init__(self, valid_area, dx, dy=None, ox=0, oy=0, offset_mode='rel', buffer=None, simplify=None):
        self.valid_area = (valid_area if isinstance(valid_area,MultiPolygon) else process_area(valid_area, buffer, simplify))
        dy = dy or dx
        self.points = self._make_grid(self.valid_area, dx, dy, ox, oy, offset_mode)


    def _test_polygons(self, valid_area, dx, dy):
        if isinstance(valid_area, Polygon): valid_area = [valid_area]
        for i,poly in enumerate(valid_area):
            xl,yl,xu,yu = poly.bounds
            w, h = (xu-xl), (yu-yl)
            if not ((w>=dx) and (h>=dy)):
                print(f'(Sub-)polygon {i+1}/{len(valid_area)} is small and may be missed by the grid. W={w}, H={h}, A={poly.area}')

    def _make_grid(self, valid_area, dx, dy, ox=0, oy=0, offset_mode='rel'):
        if offset_mode == 'abs': ox, oy = (dx*ox, dy*oy)
        elif offset_mode != 'rel': raise ValueError('`offset_mode` must be `rel` (relative) or `abs` (absolute).')
        self._test_polygons(valid_area, dx, dy)
        xl,yl,xu,yu = self.valid_area.bounds
        xs = np.arange(xl+ox, xu, dx)
        ys = np.arange(yl+oy, yu, dy)
        pts = MultiPoint([Point(x,y) for x,y in itertools.product(xs,ys)])
        pts = pts.intersection(valid_area)
        if isinstance(pts, Point):
            pts = [pts]
        else: 
            pts = list(pts)
            pts.sort(key=lambda p: p.y)
        #The intersection scrambles the order, so we sort again.
        return MultiPoint(pts)

POINTSAMPLERS = {
    'point': PointSampler,
    'centroid': CentroidPointSampler,
    'polygon': PolygonPointSampler,
    'grid': GridPointSampler,
}

def get_pointsampler(type, **kwargs):
    '''
      Get a PointSampler of type `name` (see `POINTSAMPLERS`)
      with the given kwargs.
    '''
    return POINTSAMPLERS[type](**kwargs)

def remove_intersecting(a,b):
    """
      Remove polygons from `a`, which intersect with `b`.
    """
    tree = STRtree(a)
    if isinstance(b, Polygon):
        wkbs = [c.wkb for c in tree.query(b) if c.intersects(b)]
        
    elif isinstance(b, (MultiPolygon, list)):
        wkbs = set()
        for p in b:
            for c in tree.query(p):
                if c.wkb not in wkbs and c.intersects(p):
                    wkbs.add(c.wkb)
     
    return MultiPolygon([a_ for a_ in a if a_.wkb not in wkbs])

def split_polygons(polys, train=None, val=0, test=0, buffer=0, seed=None):
    """
    Split polygons into training, validation and test sets.
    A buffer zone (`buffer`) is added around test and validation examples.
    
    Parameters
    ----------
    polys : (path, crs) or `MultiPolygon` or list of `Polygon`
      Ground-truth polygons.
    train : int
      Number of training examples (`None` gives as many as possible)
    val : int
      Number of validation polygons
    test : int
      Number of training polygons
    buffer : int
      Polygons within a buffer of this size around test-polygons
      and val-polygons are excluded.
      This avoids leakage between the train/val/test sets.
    seed : random seed.
    
    Returns
    -------
    (train_multipolygon, val_multipolygon, test_multipolygon)
    """

    if isinstance(polys,(list,tuple)) and (len(polys) == 2) and (not isinstance(polys[0],Polygon)):
        polys = process_area(polys)
    elif not isinstance(polys, MultiPolygon):
        raise ValueError("Please provide a MultiPolygon or a tuple of (path, CRS).")

    rng = np.random.default_rng(seed)
    polys = list(polys)
    rng.shuffle(polys)
    #Test polygons
    polys_test = polys[:test]
    polys_test_buf = [p.buffer(buffer) for p in polys_test] if buffer else polys_test

    #Validation polygons
    polys = polys[test:]
    polys = remove_intersecting(polys,polys_test_buf)
    polys_val = polys[:val]
    polys_val_buf = [p.buffer(buffer) for p in polys_val] if buffer else polys_val

    #Training polygons
    polys = polys[val:]
    polys = remove_intersecting(polys,polys_val_buf)
    polys_train = polys[:train]

    if (train is not None) and (len(polys) < train):
        raise RuntimeError("Could not get the requested {train} training polygons; Only {len(polys)} polygons available.")


    if (len(polys_test) < test):
        raise RuntimeError(f"Could not get the requested {test} test polygons; Only {len(polys_test)} polygons available.")
    if (len(polys_val) < val):
        raise RuntimeError(f"Could not get the requested {val} val polygons; Only {len(polys_val)} polygons available.")
    if (train is not None) and (len(polys_train) < train):
        raise RuntimeError(f"Could not get the requested {train} training polygons; Only {len(polys_train)} polygons available.")

    return MultiPolygon(polys_train), MultiPolygon(polys_val), MultiPolygon(polys_test)

def visualize_datasets(train, val, test, buffers=[500,500,500], alpha=.5):
    _, ax = plt.subplots()
    ax.set_aspect('equal', 'datalim')
    for geom in train:    
        xs, ys = geom.buffer(buffers[0]).exterior.xy    
        ax.fill(xs, ys, alpha=alpha, fc='b', ec='none')
    for geom in val:    
        xs, ys = geom.buffer(buffers[1]).exterior.xy    
        ax.fill(xs, ys, alpha=alpha, fc='r', ec='none')
    for geom in test:
        xs, ys = geom.buffer(buffers[2]).exterior.xy    
        ax.fill(xs, ys, alpha=alpha, fc='g', ec='none')
