#
# Copyright (c) Søren Rasmussen 2020 -- 2021.
# Copyright (c) The Danish Agricultural Agency 2020 -- 2021.
# This file belongs to subproject uc5a_daa_segmentation of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

from .data import *
from .data import to_tuple
from .utils import rint
import rasterio as rio
from shapely.geometry import shape, MultiPolygon, Polygon
import shapely.affinity as saff
from src.data import affine2shapely
from src.sources import VectorSrc
import shapely.geometry as sg
import fiona as fio
import cv2
from pathlib import Path
import torch as th
import numpy as np

from torchvision import transforms
from PIL import Image

def NIVA_inference_function(model):
    '''
     Default CUDA half-precision inference function
     for the NIVA project
    '''
    model = model.cuda().half()
    model.eval()
    tfm = transforms.Compose([transforms.Lambda(Image.fromarray),
                              transforms.ToTensor(),
                              transforms.Normalize([0.485, 0.456, 0.406, 0.406], [0.229, 0.224, 0.225, 0.225], inplace=True)
                             ])
    def fun(x): 
        x = tfm(x)[None]
        x = x.half().cuda()
        res = np.zeros((8, *x.shape[2:]), dtype=np.float32)
        with th.no_grad():
            for k in range(4):
                r = model(x).cpu().numpy()[0,0]
                res[k] = np.rot90(r, -k, (0,1))
                r = model(th.flip(x,(-1,))).cpu().numpy()[0,0]
                res[k+4] = np.rot90(r[...,::-1], -k, (0,1))
                x = th.rot90(x,1,(2,3))
        res = np.round(np.mean(res, 0, keepdims=False)*255).astype(np.uint8)
        #Counter edge effects
        msk = np.ones_like(res, dtype=np.bool)
        d = 128
        msk[...,:,:d] = 0
        msk[...,:,-d:] = 0
        msk[...,:d,:] = 0
        msk[...,-d:,:] = 0
        res[~msk] = 0
        
        return res, msk
    return fun

def run_inference(ds, fun, outfile):
    '''
      Run inference over a dataset and store the result in a raster
      file.
      
      Parameters
      ----------
      ds : PatchSampler
        A dataset, from which the input data is taken one image at a time.
      fun : callable.
        A function that takes a data item and returns a 1-channel prediction image
        of the same size in uint8
        Optionally, it returns a tuple where the second element is a binary mask
        defining the valid area of the prediction image.
      outfile : Output path (GTIFF format)
    '''
    dl = th.utils.data.DataLoader(ds,1, drop_last=False, num_workers=24, pin_memory=True, shuffle=False, collate_fn=lambda x: x if len(x)>1 else x[0])
    dli = iter(dl) #Spin up the data loader

    ext = ds.extent
    ih, iw = to_tuple(ds.out_hw)
    res_y, res_x = ext/ih, ext/iw
    xl,yl,xu,yu = ds.pointsampler.bounds
    xl,yl,xu,yu = xl-ext/2,yl-ext/2,xu+ext/2,yu+ext/2
    rh, rw = rint((yu-yl)/res_y), rint((xu-xl)/res_x)
    G = rio.transform.from_bounds(xl,yl,xu,yu, rw, rh)
    print("")
    res = np.zeros((rh,rw), dtype=np.uint16)
    cnt = np.zeros_like(res)
    for i,p in enumerate(dli):
        print(f"{i+1}/{len(dli)}", end="\r")
        win = rio.windows.from_bounds(*p.bounds, transform=G)
        sly,slx = rio.windows.window_index(win)
        
        pred = fun(p.read())
        if isinstance(pred, tuple):
            pred, msk = pred
        else: 
            pred, msk = res, np.ones_like(pred, dtype=np.bool)
        res[sly,slx][msk] += pred[msk]
        cnt[sly,slx][msk] += 1
    
    cnt[res==0] = 1
    res = (res / cnt.astype(np.float16)).astype(np.uint8)

    #Create raster
    profile = rio.profiles.DefaultGTiffProfile(
        count=1,
        sparse_ok=True,
        width=rw, height=rh,
        crs=ds.src.crs,
        transform=G,
        nodata=0,
        dtype=np.uint8
    )
    with rio.open(outfile, 'w', **profile) as fd:
        fd.write(res[None])
    print("\n")





def binarize_raster(rst, thr, mean_thr=None, max_thr=None, mask=None):
    '''
      Finds blobs in the raster (values higher than `thr`).
      Discards blobs where the mean value is less than `mean_thr`
      or the max value is less than `max_thr`.
    '''
    if mask is not None: rst *= mask
    _, rst_thr = cv2.threshold(rst, thr,255,cv2.THRESH_BINARY)
    contours,_ = cv2.findContours(rst_thr, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    del rst_thr

    res = []
    means = []
    maxs = []
    for c in contours:
        if c.shape[0] >= 3:
            x,y,w,h = cv2.boundingRect(c)
            win = (slice(y,y+h), slice(x,x+w))
            r = rst[win]
            c_ = c-[[[x,y]]]
            msk = cv2.drawContours(np.zeros_like(r), [c_], 0, 1, -1).astype(np.bool)
            rm = r[msk]
            if ((mean_thr is None) or (rm.mean() >= mean_thr)) and ((max_thr is None) or (rm.max() >= max_thr)):
                res.append(Polygon(c.squeeze(1)))
                means.append(rm.mean())
                maxs.append(rm.max())
    return MultiPolygon(res), means, maxs


def polygonize(file, thr=50, mean_thr=None, max_thr=127, area_thr=None, simplify=None, out=None, mask=None, verbose=False):
    '''
     Converts prediction raster (from file) to polygons and writes the
     result to `out`.

     Parameters
     ----------
     file : input file path
     thr : blob border threshold [0-255]
     mean_thr : blob mean threshold [0-255]
     max_thr : blob max threshold [0-255]
     simplify : polygon simplification (in geo-units)
     out : output file path (GPKG)
     mask : 
    '''
    if verbose: print("Reading raster... ", end="")
    with rio.open(file, 'r') as fd:
        crs = fd.profile['crs'].to_string()
        G = fd.transform
        rst = fd.read()[0]
    if verbose: print("Done")
    
    if mask is not None:
        if verbose: print("Reading mask... ", end="")
        if isinstance(mask, tuple):
            mask_file, mask_crs = mask
        else:
            mask_file, mask_crs = mask, crs
        mask_src = VectorSrc(mask_file, layer=None, crs=mask_crs)
        mask = mask_src.read(G, rst.shape)
        if verbose: print("Done")    

    if verbose: print("Binarizing raster... ", end="")
    polys, meanvals, maxvals = binarize_raster(rst, thr, mean_thr=mean_thr,max_thr=max_thr, mask=mask)
    if verbose: print("Done")
    
    if verbose: print("Transforming shapes... ", end="")
    polys = saff.affine_transform(polys, affine2shapely(G))
    if verbose: print("Done")
    
    if simplify:
        if verbose: print("Simplifying shapes... ", end="")
        polys = MultiPolygon([p.simplify(simplify) for p in polys])
        if verbose: print("Done")
    
    if area_thr is not None:
        if verbose: print("Filtering based on area... ", end="")
        polys = [p for p in polys if p.area >= area_thr]
        if verbose: print("Done")
        
    if out:
        if verbose: print("Writing output... ", end="")
        schema = dict(geometry='Polygon', properties={'area':'float','maxval':'float','meanval':'float'})
        try:
            Path(out).unlink()
        except:
            pass
        with fio.collection(out, "w", "GPKG", schema, crs=crs) as fd:
            for poly,meanval,maxval in zip(polys,meanvals,maxvals):
                d = dict(geometry=sg.mapping(poly), properties={'area':float(poly.area),'meanval':float(meanval),'maxval':float(maxval)})
                fd.write(d)
        if verbose: print("Done")
    return polys
