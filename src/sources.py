#
# Copyright (c) Søren Rasmussen 2020 -- 2021.
# Copyright (c) The Danish Agricultural Agency 2020 -- 2021.
# This file belongs to subproject uc5a_daa_segmentation of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

import matplotlib.pyplot as plt
from owslib.wms import WebMapService
from owslib.util import ResponseWrapper
import rasterio as rio
import rasterio.features as riof
import fiona as fio
from shapely.geometry import shape, MultiPolygon
from affine import Affine
import numpy as np
from .utils import *

__all__ = ['RESAMPLING', 'DataSrc', 'RasterSrc', 'WMSRasterSrc', 'VectorSrc', 'MultiSrc']

RESAMPLING = rio.enums.Resampling

def channels_last(x, squeeze=True):
    #Reorder x to channels-last
    if x.ndim == 3:
        x = x.transpose(1,2,0)
        if squeeze and x.shape[-1] == 1:
            x = x.squeeze(-1)
    return x


def tfm2bounds(G, out_hw):
    h,w = out_hw
    cnrs = np.array([[0,0],[0,h],[w,h],[w,0]])
    cnrs_g = np.array(G*cnrs.T)
    xmin,ymin = cnrs_g.min(1)
    xmax,ymax = cnrs_g.max(1)
    return (xmin,ymin,xmax,ymax)


class DataSrc():
    '''
     Data source super class
    '''
    def __init__(self, *args, **kwargs):
        raise NotImplementedError
            
    def __del__(self):
        self.close()

    def close(self):
        if hasattr(self,"ds"): self.ds.close()
        

class WMSRasterSrc(DataSrc):
    '''
      WMS Raster data source

      Parameters
      ----------
      url : String.
        WMS URL
      layer : String.
        WMS layer name
      crs : String.
        CRS
      format : String.
        Image format, e.g. `image/tiff`
      bands : List[int]
        Indexes of bands to sample. Default: all
      squeeze : Bool.
        squeeze output?
      dtype : np.dtype
        Default output dtype
      transform : function
        Default function to transform the output, e.g. `lambda x: x/np.float16(255)`

      Attributes
      ----------
      read(G, out_hw, bands=None, resampling='bilinear', squeeze=None, dtype=None, transform=None):
        Parameters
        ----------
        G : Affine
            Output geotransform
        out_hw : tuple(int,int)
            Output height, width
        bands : List[int]
            Indexes of bands to sample. Default: all
        resampling : String
            Resampling method
        squeeze : Bool.
            squeeze output?
        dtype : np.dtype
            Default output dtype
        transform : function
            Default function to transform the output, e.g. `lambda x: x/np.float16(255)`

        Returns
        -------
        Raster pixels
    '''
    def __init__(self, url, layer=None, crs="EPSG:4326", format='image/tiff', bands=None, squeeze=True, dtype=None, transform=None):
        self.wms = WebMapService(url)
        self.available_layers = self.wms.contents.keys()
        self.layer = layer
        assert layer in self.available_layers , f"Layer {layer} not found. Available layers are: {self.available_layers}"
        self.crs = crs
        self.format = format
        self.bands = bands
        self.squeeze = squeeze
        self.dtype, self.transform = dtype, transform
        
    def _req(self, bounds, out_hw):
        req = self.wms.getmap(
            layers=[self.layer], srs=self.crs, format=self.format,
            size=out_hw[::-1], bbox=bounds
        )
        return req

    def read(self, G, out_hw, bands=None, resampling='bilinear', squeeze=None, dtype=None, transform=None):
        squeeze = ifnone(squeeze, self.squeeze)
        dtype = ifnone(dtype, self.dtype)
        transform = ifnone(transform, self.transform)
        if isinstance(resampling, str): resampling = RESAMPLING[resampling]
        bounds = tfm2bounds(G, out_hw)
        req = self._req(bounds, out_hw)
        with rio.Env(GDAL_CACHEMAX=128000000):
          with rio.MemoryFile(req) as memfile:
              with memfile.open() as ds:
                  bands = bands or self.bands or list(range(1,1+ds.count))
                  bands = rio.band(ds, bands)
                  out = np.empty((len(bands.bidx),*out_hw),dtype=dtype)
                  rio.warp.reproject(bands,
                        dst_transform=G,
                        resampling=resampling,
                        destination=out,
                        dst_crs=ds.crs,
                        )
                  out = channels_last(out, squeeze=squeeze)
        if transform is not None: out = transform(out)
        return out

class RasterSrc(DataSrc):
    '''
      Raster data source

      Parameters
      ----------
      file : String.
        Raster file
      crs : String.
        CRS
      bands : List[int]
        Indexes of bands to sample. Default: all
      squeeze : Bool.
        squeeze output?
      dtype : np.dtype
        Default output dtype
      transform : function
        Default function to transform the output, e.g. `lambda x: x/np.float16(255)`

      Attributes
      ----------
      read(...) : See `WMSRasterSrc.read(...)`
    '''
    def __init__(self, file, crs="EPSG:4326", bands=None, squeeze=True, dtype=None, transform=None):
        self.file = file
        self.crs = crs
        self.bands = bands
        self.squeeze = squeeze
        self.dtype, self.transform = dtype, transform

    def read(self, G, out_hw, bands=None, resampling='bilinear', squeeze=None, dtype=None, transform=None):
        with rio.Env(GDAL_CACHEMAX=128000000):
          with rio.open(self.file, crs=self.crs) as ds:
            bands = rio.band(ds, bands or self.bands or range(1,1+ds.count))
            squeeze = ifnone(squeeze, self.squeeze)
            dtype = dtype or self.dtype or bands.dtype
            transform = ifnone(transform, self.transform)
            if isinstance(resampling, str): resampling = RESAMPLING[resampling]
                      
            out = np.empty((len(bands.bidx),*out_hw),dtype=dtype)
            rio.warp.reproject(bands,
                            dst_transform=G,
                            resampling=resampling,
                            destination=out,
                            dst_crs=ds.crs,
                            num_threads=2,
                            init_dest_nodata=False
                          ) 
        out = channels_last(out, squeeze)
        if transform is not None: out = transform(out)
        return out

    
class VectorSrc(DataSrc):
    '''
      Vector polygon data source

      Parameters
      ----------
      file : String.
        Vector file
      layer : String
        Layer name if applicable
      crs : String.
        CRS
      dtype : np.dtype
        Default output dtype
      transform : function
        Default function to transform the output, e.g. `lambda x: x/np.float16(255)`

      Attributes
      ----------
      read(G, out_hw, val=1, fill=0, dtype=None, transform=None):
        Parameters
        ----------
        G : Affine
            Output geotransform
        out_hw : tuple(int,int)
            Output height, width
        val : int
            Polygon fill value
        fill : int
            Outside fill value
        dtype : np.dtype
            Default output dtype
        transform : function
            Default function to transform the output, e.g. `lambda x: x/np.float16(255)`

        Returns
        -------
        Raster pixels
    '''
    def __init__(self, file, layer=None, crs="EPSG:4326", dtype=np.uint8, transform=None):
        self.layer = layer
        self.file = file
        self.ds = fio.open(file, crs=crs, layer=layer)
        self.crs = crs
        self.dtype, self.transform = dtype, transform

    def read(self, G, out_hw, val=1, fill=0, dtype=None, transform=None, **unused_kwargs):
        dtype = ifnone(dtype, self.dtype)
        transform = ifnone(transform, self.transform)
        bbox = tfm2bounds(G,out_hw)
        geoms = [shape(f['geometry']) for f in self.ds.filter(bbox=bbox)]
        #geoms = MultiPolygon(geoms)
        if len(geoms) > 0:
            mask = riof.rasterize(
                geoms,
                out_shape=out_hw,
                transform=G,
                fill=fill,
                all_touched=True,
                default_value=val,
                dtype=dtype
            )
        else:
            mask = fill*np.ones(out_hw, dtype=dtype)

        if transform is not None: mask = transform(mask)
        return mask
    
class MultiSrc(DataSrc):
    '''
      Combination of multiple data sources

      Parameters
      ----------
      *sources : 
        Data sources to be combined
      stack : Bool
        Stack output? (else, return a list)
      dtype : np.dtype
        Default output dtype
      transform : function
        Default function to transform the output, e.g. `lambda x: x/np.float16(255)`

      Attributes
      ----------
      read(G, out_hw, stack=None, dtype=None, transform=None) : 
        Parameters
        ----------
        G : Affine
            Output geotransform
        out_hw : tuple(int,int)
            Output height, width
        stack : bool
            Stack output? (else, return a list)
        dtype : np.dtype
            Default output dtype
        transform : function
            Default function to transform the output, e.g. `lambda x: x/np.float16(255)`

        Returns
        -------
        Raster pixels
    '''
    def __init__(self, *sources, stack=True, dtype=None, transform=None):
        if len(sources) == 1 and isinstance(sources[0], (list, tuple, set)):
            sources = sources[0]
        self.sources = list(sources)
        for s in sources[1:]: assert s.crs == sources[0].crs
        self.stack = stack
        self.dtype, self.transform = dtype, transform
    
    @property
    def crs(self): return self.sources[0].crs
    def read(self, G, out_hw, stack=None, dtype=None, transform=None, **kwargs):
        stack = ifnone(stack, self.stack)
        dtype = ifnone(dtype, self.dtype)
        transform = ifnone(transform, self.transform)
        out = [s.read(G, out_hw, **kwargs) for s in self.sources]
        if stack: out = np.dstack(out)
        if transform is not None: out = transform(out)
        if dtype is not None: out = out.astype(dtype)
        return out

    def __getitem__(self, key):
        return self.sources[key]
    def __setitem__(self, key, val):
        self.sources[key] = val
