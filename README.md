November 2020  
Søren Rasmussen - Alexandra Instituttet A/S  
soren.rasmussen@alexandra.dk  


This text documents the use of the system for automatic segmentations of certain agriculture related elements in land parcels. The system was developed as part of the NIVA project, funded by the European Union.

This work is licensed under the EUPL

# Introduction

This sub-project is part of the ["New IACS Vision in Action”
--- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for
e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate
data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under
grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on
[gitlab](https://gitlab.com/nivaeu/)

The end-goal of this project is to create a system which can segment elements (e.g. lakes/ponds, groups of trees, etc.) in aerial photos (RGB+NIR) and produce vector outputs.
This is achieved via the use of neural networks (NNs).
The NNs are trained using aerial photos and corresponding vector annotations.
Once trained, the NNs produce outputs, which are processed into the final vector outputs.

# Requirements
## Hardware
* x86_64 PC
* 32GB RAM (may work with less)
* CUDA capable GPU, 8GB RAM (Compute Capability 3.0+)

## Software
* Linux operating system (may work with other systems)
* NVIDIA driver 440.33.01 (may work with other versions)
* CUDA 10.02
* Docker 19.03.12 (may work with other versions)

## Setup
The system runs in a Dockerized environment. To use it:

1. Navigate to the `docker` directory in a terminal.
1. Run: `./build.sh` to build the docker image (only do this once).
1. Run: `./run.sh jupyter notebook` to start the environment and run a Jupyter Notebook server.
1. Follow instructions in the terminal access the Jupyter Notebook.

# Training the neural networks
Training the neural networks is a two-step process:

1. Self-supervised pre-training (without annotations).
2. Supervised fine-tuning (using annotations)

## Unsupervised pre-training
The Unsupervised pre-training uses the approach from 

> Grill et al. "Bootstrap Your Own Latent-A New Approach to Self-Supervised Learning." Advances in Neural Information Processing Systems 33 (2020).

This teaches the NN to recognize general features in aerial photos, and forms a basis for supervised fine-tuning.
Refer to the Jupyter Notebook `train_unsupervised.ipynb` for details and an example of how to perform this training.

### Training data
Unsupervised pre-training utilizes the following data:

1. Aerial photo raster: 4-channel RGB + NIR, e.g. in a GeoTiff.
3. Polygons indicating the valid area for sampling of training images. GPKG format.

## Supervised fine-tuning
Using the pre-trained weights as a starting point, NNs are tuned to solve specific segmentation tasks.
Refer to the Jupyter Notebook `tune_supervised.ipynb` for details and an example of how to perform this training.

### Training data
Supervised training utilizes the following data:

1. Aerial photo raster: 4-channel RGB + NIR, e.g. in a GeoTiff.
2. Ground truth polygon annotations. GPKG format.
3. (Optional) Polygons indicating the valid area for predictions, e.g. field polygons. GPKG format.


# Inference
The NNs each output a continous-valued raster image conveying the confidence that a given pixel position in the input image corresponds to the element type of interest.
The continous-valued outputs are then processed into binary-valued vector maps, thus forming the final output.
Refer to the Jupyter Notebook `inference.ipynb` for details and examples.

1. Aerial photo raster: 4-channel RGB + NIR, e.g. in a GeoTiff.
3. Polygons indicating the valid area for inference. GPKG format.
