#!/bin/bash

#
# Copyright (c) Søren Rasmussen 2020 -- 2021.
# Copyright (c) The Danish Agricultural Agency 2020 -- 2021.
# This file belongs to subproject uc5a_daa_segmentation of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

HOMEDIR_HOST="$SCRIPTPATH/home"
HOMEDIR_CONT="/home/vclab"
CODEDIR_HOST="$SCRIPTPATH/.."
CODEDIR_CONT="/lbst"
#DATADIR_HOST="/media/sras/5TB_USB"
#DATADIR_CONT="/data"

WORKDIR="$CODEDIR_CONT"

#  --mount type=bind,source=$DATADIR_HOST,target=$DATADIR_CONT \  --mount type=bind,source=$SSDDIR_HOST,target=$SSDDIR_CONT \
docker run --gpus 1 --rm -it \
  --name=lbst \
  --mount type=bind,source=$HOMEDIR_HOST,target=$HOMEDIR_CONT \
  --mount type=bind,source=$CODEDIR_HOST,target=$CODEDIR_CONT \
  --workdir=$WORKDIR \
  --shm-size=64G \
  --network host \
  vclab/lbst \
  $*
