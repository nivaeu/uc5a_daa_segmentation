{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Unsupervised pre-training\n",
    "This notebook shows the process of pre-training a neural network using an unsupervised approach."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Load modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "from pytorch_lightning import LightningDataModule\n",
    "from src.data import PatchPairSampler\n",
    "from src.sources import RasterSrc\n",
    "from src.utils import *\n",
    "from src.model import *\n",
    "import numpy as np\n",
    "import torch as th\n",
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Define the dataset\n",
    "The PatchPairSampler samples from our raster `src=RGBN` and produces a pair of (partially) overlapping patches.\n",
    "The locations of the samples are determined by the PointSampler, which is configured via `pointsampler_spec`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configure Input data source\n",
    "Rasters can be loaded from a local source or from WMS. WMS will most likely be slow for training. Refer to `src/sources.py` for other types of sources.\n",
    "In this case, we load a 4 channel (RGB+NIR raster)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "crs = \"EPSG:25832\"\n",
    "RGBN = RasterSrc(\"/dev/shm/sop19_rgbn.tif\", crs=crs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data augmentation functions\n",
    "It is assumed that the input is a 4-channel image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "import cv2\n",
    "import numpy as np\n",
    "import torchvision.transforms as transforms\n",
    "from PIL import Image\n",
    "\n",
    "\n",
    "input_size = 128\n",
    "color_jitter = transforms.ColorJitter(0.4, 0.4, 0.4, 0.1)\n",
    "\n",
    "class GaussianBlur(object):\n",
    "    #Random gaussian blur\n",
    "    def __init__(self, kernel_size, min=0.1, max=2.0):\n",
    "        self.min = min\n",
    "        self.max = max\n",
    "        self.kernel_size = (kernel_size//2)*2+1\n",
    "\n",
    "    def __call__(self, sample):\n",
    "        prob = np.random.random_sample()\n",
    "\n",
    "        if prob < 0.5:\n",
    "            is_PIL = isinstance(sample, Image.Image)\n",
    "            sample = np.array(sample)\n",
    "            sigma = (self.max - self.min) * np.random.random_sample() + self.min\n",
    "            sample = cv2.GaussianBlur(sample, (self.kernel_size, self.kernel_size), sigma)\n",
    "            if is_PIL: sample = Image.fromarray(sample)\n",
    "        return sample\n",
    "\n",
    "def patchpair_transform(pp):\n",
    "    \n",
    "    img_transforms = transforms.Compose([transforms.Lambda(Image.fromarray),\n",
    "                                         transforms.RandomApply([color_jitter], p=0.5),\n",
    "                                         GaussianBlur(kernel_size=int(0.1 * input_size), min=.1, max=1),\n",
    "                                         transforms.ToTensor(),\n",
    "                                         transforms.Normalize([0.485, 0.456, 0.406, 0.406], [0.229, 0.224, 0.225, 0.225], inplace=True)\n",
    "                                     ])\n",
    "    \n",
    "    img_a, img_b, _ = pp.read()\n",
    "    img_a, img_b = img_transforms(img_a), img_transforms(img_b)\n",
    "    return img_a, img_b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configure the patch pair sampler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Specification of the point sampling mechanism\n",
    "pointsampler_spec = dict(\n",
    "    type       = 'polygon',                 #Samples will be taken from the interior of a set of polygons.\n",
    "    valid_area = (\"data/dk_poly.gpkg\",crs), #Path and CRS of the polygon dataset.\n",
    "    buffer     = -100,                      #The polygons will be buffered by -100 meters to avoid most of the shore line.\n",
    "    simplify   = 50                         #Simplifying the resulting polygons to be accurate within 50m makes sampling faster.\n",
    ")\n",
    "\n",
    "psamp = PatchPairSampler(\n",
    "    src=RGBN,                           #Raster source\n",
    "    out_hw=128,                         #Output patch size\n",
    "    pointsampler_spec=pointsampler_spec,#PointSampler specification, as defined above\n",
    "    rotation=(0,360),                   #Patches may be rotated in the range [0,360] deg.\n",
    "    extent=(200,250),                   #The geographical extent of patches vary from 200 to 250 meters on each axis\n",
    "    shear=(-20,20),                     #Patches are sheared by -20 to 20 degrees\n",
    "    translation=(-20,20),               #Patches are translated from the sampled center point by -20 to 20 meters\n",
    "    len=5000,                           #A fake dataset length to define the length of an \"epoch\", even though we can take infinitely many samples\n",
    "    flip=.5,                            #Patches are flipped on each axis with 50% probability\n",
    "    transform=patchpair_transform       #Final transform (augmentation and tensor conversion)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize the sampling area"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "psamp.pointsampler.valid_area"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create data loader\n",
    "Parallel loading and batching of patch pairs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dl = th.utils.data.DataLoader(psamp,80, drop_last=True, num_workers=12, pin_memory=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load model and self-supervised wrapper"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dlab = byol_deeplabv3()\n",
    "module = BYOL(dlab, learning_rate = 1e-5, weight_decay=5e-6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_dir = \"runs/unsupervised/\" #Ensure that this directory exists\n",
    "trainer = pl.Trainer(gpus=[0],\n",
    "                     max_steps=1000*len(psamp),\n",
    "                     precision=16,\n",
    "                     default_root_dir = run_dir,\n",
    "                    )\n",
    "trainer.checkpoint_callback.save_last = True\n",
    "trainer.fit(module,train_dataloader=dl)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
